<%-- 
    Document   : assignRoom
    Created on : Apr 27, 2017, 11:49:57 PM
    Author     : boom
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AssignRoom</title>
        <style>
            .head{
                 text-align: center;
                 
            }
            body{
                 background-color: cornflowerblue;
            }
            .bgcontent{
                background-color: paleturquoise;
                width: 50%;
            }
            .title{
                text-align: right;
            }
            th{
                 text-align: center;
                  background-color: darkgrey;
         
            }
            table.Result td{
                background-color: aliceblue;
            }
        </style>
            
    </head>
    <body>
        <h1 class="head">โครงการส่งเสริมวิชาโอลิมปิกวิชาการ</h1>
        
        <div class="bgcontent">
            <form action="" method="post">
                <table cellspacing="30">
                    <tr>
                        <td class="title">เลขบัตรประชาชนผู้เข้าพัก :</td>
                        <td><input type="text" name="idCard"></td><br>
                    </tr>
                    <tr>
                        <td class="title">เลขบัตรประชาชนผู้เข้าพักร่วม :</td>
                        <td><input type="text" name="co_idCard"></td>
                    </tr>
                    <tr>
                        <td class="title">ชื่ออาคาร :</td>
                        <td>
                            <select name="buildname">
                                <option value="">อาคารD1</option>                                
                            </select>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="title">เลขที่ห้อง :</td>
                        <td>
                            <select name="roomNo">
                                <option value="">1201</option>                                
                            </select>
                        </td>              
                    </tr>
                </table>
                <div class="head"><button type="submit">บันทึก</button></div>
            </form>
        </div>
        
        <h3>จำนวนผู้พัก:</h3>
        
         <table class="Result">
     <tr>
             <th>
                เลขบัตรประชาชน
             </th>
             <th>
                 ชื่อ-นามสกุล
             </th>
            <th>
                 เลขบัตรประชาชนผู้พักร่วม
            </th>
            <th>
                  ชื่อ-นามสกุลผู้พักร่วม
            </th>
             <th>
                  ชื่ออาคาร
            </th>
            <th>
                  เลขห้อง
            </th>
        
     </tr>
    <tr>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>      
    </tr>
 </table>
    </body>
</html>
