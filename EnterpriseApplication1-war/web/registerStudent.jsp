<%-- 
    Document   : registerStudent
    Created on : Apr 25, 2017, 10:32:06 PM
    Author     : boom
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ลงทะเบียนนักเรียน</title>
        <style>
            .head{
                  text-align: center;
            }
            .title{
                  text-align: right
            }
            .bgcontent{
                background-color: paleturquoise
            }
            body{
                background-color: cornflowerblue;
            }
           
            
        </style>
    </head>
    <body>
        <h1 class="head">โครงการส่งเสริมวิชาโอลิมปิกวิชาการ</h1>
        <h3>ลงทะเบียนนักเรียน</h3>
      <div class="bgcontent">
        <form method="post" action="">
            <table cellspacing="30">
                <tr>
                    <td class="title">
                          เลขที่บัตรประชาชน :  
                    </td>
                    <td>
                        <input type="text" name="idCard" value="">                                          
                    </td>
                </tr>
                <tr>
                    <td class="title">
                       คำนำหน้าชื่อ :  
                       </td>
                       <td>
                        <select name="title">
                            <option value="ด.ช">ด.ช</option>
                            <option value="ด.ญ">ด.ญ</option>
                        </select>
                       </td>                  
                    <td class="title">
                        ชื่อ* :             
                    </td>
                    <td>
                        <input type="text" name="firstName" value="">
                        
                    </td>
                    <td class="title">
                        นามสกุล* :    
                    </td>
                    <td>
                        <input type="text" name="lastName" value="">
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        ชื่อโรงเรียน* :     
                    </td>
                    <td>
                        <input type="text" name="schoolName" value="">
                    </td>
                    <td class="title">
                        เบอร์โทร :       
                    </td>
                    <td>
                        <input type="text" name="phone" value="">
                    </td>
                      <td class="title">
                        Email :       
                      </td>
                      <td>
                        <input type="email" name="email" value="">
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        ชื่อ-นามสกุลผู้ปครอง :     
                    </td>
                    <td>
                        <input type="text" name="parentName" value="">
                    </td>
                    <td class="title">
                        เบอร์โทรผู้ปกครอง :    
                    </td>
                    <td>
                        <input type="text" name="parentPhone" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        สถานะ* : <br> <input type="radio" name="status" value="นักเรียนเข้าค่าย"> นักเรียนเข้าค่าย <br>
                                <input type="radio" name="status" value="นักเรียนขอใช้สิทธิสอบ"> นักเรียนขอใช้สิทธิสอบ<br>
                                <input type="radio" name="status" value="อาจารย์ผู้ดูแล"> อาจารย์ผู้ดูแล                          
                    </td>
                </tr>
            </table>
            <div class="head" >
            <button type="submit">เพิ่ม</button> 
            <button type="reset">ยกเลิก</button>
            </div>
            
        </form>
      </div>
       <%@ include file="queryStudent.jspf" %> 
    </body>
</html>
