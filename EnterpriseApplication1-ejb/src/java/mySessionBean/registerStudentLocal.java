/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mySessionBean;

import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import myEntityClass.Parent;
import myEntityClass.Participant;

/**
 *
 * @author boom
 */
@Local
public interface registerStudentLocal {
    
     void addStudent(HashMap student);
     void addParent(String fname, String lname, String phone);
     List<Participant> getAllStudent();
     Parent getParent(String cardId);
    
}
